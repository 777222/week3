package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class AssignmentOne {
    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("/Users/ashimdauren/Desktop/Calik/Task Student/src/com/company/file1.txt");
        Scanner sc1 = new Scanner(file);

        Shape s = new Shape();
        ArrayList<Point> points = s.getPoints();

        while (sc1.hasNextInt()) {
            Point newPoint = new Point(sc1.nextInt(), sc1.nextInt());
            s.addPoint(newPoint);
        }
        for (int i = 0; i < points.size(); i++)
            System.out.println(points.get(i).getX() + ", " + points.get(i).getY() );
        for (int i = 0; i < points.size(); i++) {
            if (i < points.size() - 1)
                System.out.println("Dis <-> " + (i + 1) + " and " + (i + 2) + " points = " +
                        points.get(i).distance(points.get(i + 1)));
            else System.out.println("Dis <-> " + (i + 1) + " and " + 0 + " points = " +
                    points.get(i).distance(points.get(0)));
        }
        System.out.println("Perimeter : " + s.calculatePerimeter());
        System.out.println("Longest length : " + s.longestSide());
        System.out.println("Average length : " + s.averageLength() );

        points.get(0).setX(500);
        points.get(0).setY(-500);
        System.out.println(points.get(0).getX() + " " + points.get(0).getY());

    }
}