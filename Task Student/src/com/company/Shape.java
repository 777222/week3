package com.company;
import java.util.ArrayList;

public class Shape {

    private ArrayList<Point> points;

    Shape() {
        points = new ArrayList<Point>();
    }

    void addPoint(Point p) {
        points.add(p);
    }

    ArrayList<Point> getPoints() {
        return points;
    }

    double calculatePerimeter() {
        double per = 0;

        for (int i = 0; i < points.size(); i++) {
            if (i < points.size() - 1) per += points.get(i).distance(points.get(i + 1));
            else per += points.get(i).distance(points.get(0));
        }

        return per;
    }

    double longestSide() {
        double longest = points.get(0).distance(points.get(1));

        for (int i = 1; i < points.size(); i++) {
            if (i < points.size() - 1 && points.get(i).distance(points.get(i + 1)) > longest)
                longest = points.get(i).distance(points.get(i + 1));
            else if (i == points.size() - 1 && points.get(i).distance(points.get(0)) > longest)
                longest = points.get(i).distance(points.get(0));
        }

        return longest;
    }

    double averageLength() {

            double average = 0;

            for (int i = 0; i < points.size(); i++) {
                if (i < points.size() - 1) average += points.get(i).distance(points.get(i + 1));
                else average += points.get(i).distance(points.get(0));
            }
        return calculatePerimeter() / points.size();

    }
}