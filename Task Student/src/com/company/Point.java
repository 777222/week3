package com.company;

public class Point {
    private int x, y;

    Point(int x, int y) {
        setX(x);
        setY(y);
    }
    void setX(int x) {
        this.x = x;
    }
    int getX() {
        return x;
    }
    void setY(int y) {
        this.y = y;
    }
    int getY() {
        return y;
    }
    double distance(Point p) {
        return Math.sqrt(Math.pow(p.x - this.x, 2) + Math.pow(p.y - this.y, 2));
    }
}