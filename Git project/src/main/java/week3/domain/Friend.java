package week3.domain;

import java.util.ArrayList;
import java.util.Collections;

public class Friend  extends User {
    private static ArrayList<ArrayList> friendList = new ArrayList();
    private static ArrayList<User> conList = new ArrayList();

    public Friend(String  firstname, String secondname, String username, Password password) {
        super(firstname, secondname, username, password);
    }
    public void addFriend(User u1,User u2){
        conList.add(u1 );
        conList.add(u2);
        friendList.add(conList);
    }
    public void showFriends(User user){
        for(int i=0;i<friendList.size();i++){
            if (friendList.get(i).get(0).equals(user)){
                System.out.println(friendList.get(i).get(1));
            }
        }
    }

}
