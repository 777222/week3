package week3.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class MyApplication {
    private static ArrayList<User> users;
    private Scanner sc = new Scanner(System.in);
    private User signedUser=null;
    public MyApplication() throws FileNotFoundException {
        users=new ArrayList<>();
    }
    private void addUsers() throws FileNotFoundException {
        File file =new File("/Users/ashimdauren/Desktop/Calik/Git project/src/main/java/week3/domain/db.txt");
        Scanner fsc = new Scanner(file);
        while (fsc.hasNextInt()){
            int id = fsc.nextInt();
            String name = fsc.next();
            String surname = fsc.next();
            String username = fsc.next();
            Password password =new Password(fsc.next());
            User user = new User(id, name, surname, username, password);
            users.add(user);
        }
    }



    private void menu() {
        while (true) {
            if (signedUser == null) {
                System.out.println("You are not signed in.");
                System.out.println("1. Authentication");
                System.out.println("2. Exit");
                int choice = sc.nextInt();
                if (choice == 1) authentication();
                else break;
            }
            else {
                userProfile(signedUser);
            }
        }
    }

    private void userProfile(User user) {
        while(true) {
            System.out.println(user.toString());
            int choice = sc.nextInt();
            System.out.println("1. send mes");
            System.out.println("1. log off");
            if (choice == 1){
                System.out.println("Enter username");
                Scanner sc3 = new Scanner(System.in);
                String username= sc3.next();

            }
            if (choice == 2) {logOff();}
            else break;
        }

    }

    private void logOff() {
        signedUser = null;
        menu();
    }

    private void authentication() {
        while (true) {
            System.out.println("1. Log in");
            System.out.println("2. Registration");
            System.out.println("3. Back");
            int choice = sc.nextInt();
            if (choice == 1) logIn();
            else if(choice == 2) reg();
            else break;
        }
        menu();
    }


    private void logIn() {
        Scanner sc2=new Scanner(System.in);
        System.out.println("Enter username");
        String username = sc2.next();
        System.out.println("Enter password");
        Password password = new Password(sc2.next());
        for (User user : users){
            if(user.getUsername().equals(username)){
                if (checkPassword(password,user)){
                    signedUser = user;
                    menu();
                    return;
                }
                else{
                    System.out.println("Error: incorrect username or password");
                    return;
                }
            }
        }
        System.out.println("Sorry we can't found this username");
    }
    private void reg() {
        Scanner sc1=new Scanner(System.in);
        System.out.println("New user creation");
        System.out.println("Enter First name:");
        String name =sc1.next();
        System.out.println("Enter Second name");
        String surname = sc1.next();
        System.out.println("Enter login");
        String username = sc1.next();
        System.out.println("Enter password");
        Password password = new Password(sc1.next());
        while (!checkUsername(username)){
            System.out.println("Sorry this username is already registrated");
            username=sc1.next();
        }

    }

    public static boolean checkUsername(String username) {
        for (User user : users){
            if(user.getUsername().equals(username)){
                return false;
            }
        }
        return true;
    }
    public static boolean checkPassword(Password password,User user) {
        if (user.getPassword().equals(password)) {
            return true;
        } else {
            return false;
        }
    }
    public void start() throws IOException {
        // fill userlist from db.txt
        addUsers();
        while (true) {
            System.out.println("Welcome to my application!");
            System.out.println("Select command:");
            System.out.println("1. Menu");
            System.out.println("2. Exit");
            int choice = sc.nextInt();
            if (choice == 1) {
                menu();
            } else {
                break;
            }
        }

        saveUserList();
    }

    private void saveUserList() throws IOException {
        String content="";
        for (User user : users){
            content+=user.getId()+" "+user.getName()+" "+user.getSurname()+
                    " "+user.getUsername()+" "+user.getPassword()+"\n";
        }
        Files.write(Paths.get("/Users/ashimdauren/Desktop/Calik/Git project/src/main/java/week3/domain/db.txt"),content.getBytes()    );
    }
}
